package run.yang.androidstudiotricks.editing;

/**
 * Created by ty on 3/14/17.
 */

public class UserInfoManager {

  private static class InstanceHolder {
    static final UserInfoManager sInstance = new UserInfoManager();
  }

  public static UserInfoManager getInstance() {
    return InstanceHolder.sInstance;
  }

  private static volatile UserInfoManager sInstance = null;

  private UserInfoManager() {

  }
}

