package run.yang.androidstudiotricks.editing;

/**
 * Created by ty on 3/6/17.
 */

public interface IGetUserInfo {
  String getUserName();
}
