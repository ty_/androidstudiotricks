package run.yang.androidstudiotricks.editing;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.location.Location;

/**
 * 创建时间: 2017/02/28 16:30 <br>
 * 作者: Yang Tianmei <br>
 * 描述:
 */

public class TextEditing {
  // =============================
  // =============================
  // 移动光标/选中文字（所有文字编辑器都支持的快捷键）
  //
  // Ctrl + Left/Right: 逐词移动光标
  // Ctrl + Shift + Left/Right：逐词选择文字
  // Ctrl + Backspace：删除光标前一个单词
  // Ctrl + Del：删除光标后一个单词
  // Home/End：移动光标到行首/行尾
  // Shift + Home/End：选中当前位置到到行首/行尾的文字
  public static void moveCursor移动光标(Context context) {
    ApplicationInfo applicationInfo = context.getApplicationInfo();
    String[] sharedLibraryFiles = applicationInfo.sharedLibraryFiles;
    if (sharedLibraryFiles != null) {
      for (String sharedLibraryFile : sharedLibraryFiles) {
        System.out.println(sharedLibraryFile);
      }
    }
  }

  // =============================
  // =============================
  // Ctrl + D：Duplicate Line
  // Ctrl + Y：Delete Line
  // Ctrl + W：Extend Selection（按多次），逐步扩大选择范围
  public static void copyDeleteUndoRedo复制删除(int index) {
    for (int i = 0; i < index; i++) {
      System.out.println(i);
    }
  }

  // =============================
  // =============================
  // Basic Completion: Ctrl + Space
  // SmartType: Ctrl + Shift + Space可按两次)
  public static Drawable smartTypeCompletion智能补全(Context context, Resources res,
      Drawable oldDrawable, int resId) {
    //Drawable drawable =

    // 只有一个候选项时不需要选择
    //Location location =

    //String[] names = { "yang", "xi" };
    //List<String> list =

    return null;
  }

  private static Location getCurrentLocation() {
    return null;
  }

  // =============================
  // =============================
  // 让Android Studio 识别变量 m 和 s 前缀
  // Settings -> Editor -> Code Style -> Java -> Code Generation
  // Name prefix: Field 填：m，Static Field 填：s

  // 1. Quick Fix: Alt + Enter, 快速创建构造函数初始化 final 成员
  // 2. 重载 openOrCreateDatabase, SmartType: Ctrl + Shift + Space
  static abstract class ContextWrapper extends Context {

  }

  // =============================
  // =============================
  // Complete current statement: Ctrl + Shift + Enter
  public static void completeCurrentStatement() {
    // 1. 定义函数

    // 2. 调用带参数的函数
    joinLines合并行("s", 12, false);

    // 3. 数学表达式
    //final double a = 5, b = 12;
    //double c = Math.sqrt(a * a + b * b);
  }

  // =============================
  // =============================
  // Ctrl + Shift + J： 合并行 Join Lines
  public static void joinLines合并行(String tag, int inputScore, boolean printTime) {
    // A
    // comment
    String name;
    name = "Yang";

    int score = inputScore;
    score -= 10;
  }

  // =============================
  // =============================
  // 使用 Tab 键替换
  public static boolean useTabToReplace使用Tab键替换(String first, String second) {
    //return first.contentEquals(second);
    return first.equals(second);
  }

  // =============================
  // =============================
  // Ctrl + Shift + Alt + C
  // Ctrl + Shift + C
  public static void copyReference() {

  }

  // =============================
  // =============================
  public static Context context = null;

  // Shift + F6: Rename
  public static void rename(String userId) {
    System.out.println("irrelevant function with same name: " + getUserName());

    IGetUserInfo userInfo = null;
    String userName = userInfo.getUserName();
  }

  // 重命名干扰项，名称相同的函数
  private static String getUserName() {
    return "yang";
  }
}
